/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.ResultSet;
import java.util.ArrayList;
/**
 *
 * @author Acer
 */
public class Account {
    private int accountNumber;
    private int pin;
    private String date;
    private double balance;
    
    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    public int getAccountNumber() {
        return accountNumber;
    }
    
    public void setPin(int pin) {
        this.pin = pin;
    }
    
    public int getPin() {
        return pin;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    public double getBalance() {
        return balance;
    }
    
    public String getDate() {
        return date;
    }

    public boolean validateAccount(){
        String query = "select number from account where number = ? and pin = ?";
        MySQLConnection con = new MySQLConnection();

        ArrayList<Object> param=new ArrayList<Object>();
        param.add(this.accountNumber);
        param.add(this.pin);

        try{
            ResultSet resultSet = con.preparedStatement(query, param);

            if(resultSet.next()){
                return true;
            }else{
                return false;
            }
        }catch(Exception ex){
            
            return false;
        }
    }
    
    public void getBalanceAccount(){
        
        String query = "select balance from account where number = ?";
        MySQLConnection con = new MySQLConnection();

        ArrayList<Object> param=new ArrayList<Object>();
        param.add(accountNumber);

        try{
            ResultSet resultSet = con.preparedStatement(query, param);

            Transaction transaction = new Transaction();
            transaction.save(2, this.accountNumber,0);
            
            if(resultSet.next()){
                this.balance = resultSet.getDouble("balance"); 
            }
        }catch(Exception ex){
            
        }
    }
    
    public boolean addBalance(){
        String query = "update account set balance = balance + ? where number = ?";
        MySQLConnection con = new MySQLConnection();
        
        ArrayList<Object> param=new ArrayList<Object>();
        param.add(this.balance);
        param.add(this.accountNumber);
        
        try{
            ResultSet resultSet = con.preparedStatement(query, param);
            
            Transaction transaction = new Transaction();
            transaction.save(3, this.accountNumber, this.balance);
            
            return true;
            
        }catch(Exception ex){
            return false;
        }
    }
    
    public boolean withdraw(){
       String query = "update account set balance = balance - ? where number = ?";
        MySQLConnection con = new MySQLConnection();
        
        ArrayList<Object> param=new ArrayList<Object>();
        param.add(this.balance); 
        param.add(this.accountNumber);

        try{
            ResultSet resultSet = con.preparedStatement(query, param);
            Transaction transaction = new Transaction();
            transaction.save(2, this.accountNumber, this.balance);
            
            return true;
            
        }catch(Exception ex){
            return false;
        } 
    }

}
